/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ulises;

/**
 *
 * @author Legend Code
 */
public class Pizza {
    //Crear Atributos privados
    private String nombre, tamano, masa;
    
    //Construir metodos(customer)
    public void preparar(){
        System.out.println("estamos prepaparando tu pizza");
    }
    
    public void calentar(){
        System.out.println("estamos calentando tu pizza");
    }
    //04 Crear constructor con y sin parámetro

    public Pizza() {
    }

    public Pizza(String nombre, String tamano, String masa) {
        this.nombre = nombre;
        this.tamano = tamano;
        this.masa = masa;
    }
    
    //05 Crear método getter y setter

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public String getMasa() {
        return masa;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }
    
    //06 Crear metodo toString
    //Con este método traigo el objeto con todos sus atributos

    @Override
    public String toString() {
        return "Pizza{" + "nombre=" + nombre + ", tamano=" + tamano + ", masa=" + masa + '}';
    }
    
}
